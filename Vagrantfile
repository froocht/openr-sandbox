# Open/R sandbox built by Felix Stolba
#
# You have to set the interface names to map to your host-only networks
$segment_a = 'VirtualBox Host-Only Ethernet Adapter #4'
$segment_b = 'VirtualBox Host-Only Ethernet Adapter #3'
$segment_c = 'VirtualBox Host-Only Ethernet Adapter #5'
#
# Credit: This is the advanced Vagrant tutorial which helped me set this up
# http://buildvirtual.net/using-vagrant-to-deploy-multiple-vms-on-vsphere/
#######################################################################

$provision_routers = <<-SCRIPT
# common stuff
hostnamectl set-hostname $NAME
apt-get update

sysctl net.ipv6.conf.all.forwarding=1

/sbin/ifdown enp0s8
# interface config
echo """
auto enp0s8
iface enp0s8 inet6 static
        address $ETH0_IP6
        netmask 64
""" >> /etc/network/interfaces
/sbin/ifup enp0s8


if [ $NAME != 'CMDR' ]; then 
#### This is for the routing nodes
    apt-get --no-install-recommends install -y libdouble-conversion-dev \
      libssl-dev \
      libtool \
      libboost-all-dev \
      libevent-dev \
      liblz4-dev \
      liblzma-dev \
      scons \
      libkrb5-dev \
      libsnappy-dev \
      libsasl2-dev \
      libnuma-dev \
      zlib1g-dev \
      libjemalloc-dev \
      libiberty-dev 

    /sbin/ifdown enp0s9
    /sbin/ifdown lo

    # interface config
    echo """
    auto enp0s9
    iface enp0s9 inet6 static
          address $ETH1_IP6
          netmask 64

    """ >> /etc/network/interfaces
    /sbin/ifup enp0s9
    /sbin/ifup lo

    sleep 5

    # install openr
    tar xfvz /vagrant/openr-*.tgz -C /
    echo /usr/local/lib > /etc/ld.so.conf.d/openr.conf
    ldconfig

    # set as autostart task
    echo '#!/bin/sh' > /etc/rc.local
    echo """openr --advertise_interface_db=false \
    --assume_drained=false \
    --config_store_filepath=/tmp/aq_persistent_config_store.bin \
    --decision_debounce_max_ms=250 \
    --decision_debounce_min_ms=10 \
    --domain=openr \
    --dryrun=false \
    --enable_fib_sync=true \
    --enable_health_checker=false \
    --enable_netlink_fib_handler=true \
    --enable_netlink_system_handler=true \
    --enable_perf_measurement=true \
    --enable_prefix_alloc=true  \
    --enable_segment_routing=false  \
    --enable_v4=false  \
    --fib_handler_port=60100  \
    --health_checker_ping_interval_s=3 \
    --loopback_iface=lo  \
    --ifname_prefix=en  \
    --iface_regex_exclude=  \
    --iface_regex_include=enp0s[89]  \
    --node_name=`hostname` \
    --override_loopback_addr=true  \
    --prefixes=  \
    --redistribute_ifaces=lo  \
    --seed_prefix=2001:db8:ffff:ffff::/64  \
    --alloc_prefix_len=128 \
    --set_loopback_address=true \
    --spark_fastinit_keepalive_time_ms=100  \
    --spark_hold_time_s=30  \
    --spark_keepalive_time_s=3  \
    --static_prefix_alloc=false \
    --enable_rtt_metric=true  \
    --enable_watchdog=true  \
    --logbufsecs=0  \
    --logtostderr  \
    --max_log_size=1  \
    --v=1 &""" >> /etc/rc.local
    echo 'exit 0' >> /etc/rc.local
    systemctl enable rc-local
    systemctl stop rc-local && systemctl start rc-local

else
####This is for mgmt station

    /sbin/ifdown enp0s8
    echo "        up route add -A inet6 default gw 2001:db8::1" >> /etc/network/interfaces
    /sbin/ifup enp0s8

    apt-get --no-install-recommends install -y python-pip

    # install module prereqs
    for i in setuptools six future typing; do
      pip install $i
    done
    export BASEDIR=/vagrant/py
    cd $BASEDIR
    for n in `ls -1 *.tar.gz`; do tar xfvz $n -C /; done

    # install the rest
    pip install -r $BASEDIR/requirements.txt

    echo "2001:db8::1    r01" >> /etc/hosts
    echo "2001:db8::2    r02" >> /etc/hosts
    echo "2001:db8::3    r03" >> /etc/hosts

fi

SCRIPT


# v6 prefix defaults to /64
vms = [
    { 'n' => '1',  'hostname' => 'R01',  'net1' => $segment_b, 'prefix1_v6' => '2001:db8::', 'net2' => $segment_a, 'prefix2_v6' => 'fd92:a:b:c::'  },
    { 'n' => '2',  'hostname' => 'R02',  'net1' => $segment_b, 'prefix1_v6' => '2001:db8::', 'net2' => $segment_a, 'prefix2_v6' => 'fd92:a:b:c::'  },
    { 'n' => '3',  'hostname' => 'R03',  'net1' => $segment_b, 'prefix1_v6' => '2001:db8::', 'net2' => $segment_c, 'prefix2_v6' => 'fd92:a:b:1e::' },
    { 'n' => '23', 'hostname' => 'CMDR', 'net1' => $segment_b, 'prefix1_v6' => '2001:db8::', 'net2' => '',         'prefix2_v6' => ''                }
]

Vagrant.configure("2") do |config|
    vms.each do |node|
        config.vm.define node['hostname'] do |node_config|

            node_config.vm.box = "ubuntu/xenial64"
            node_config.vm.network "private_network", auto_config: false,
                virtualbox__intnet: node['net1']
            if node['net2'] != ''
            node_config.vm.network "private_network", auto_config: false,
                virtualbox__intnet: node['net2']
            end

            node_config.vm.provision "shell", inline: $provision_routers, 
            env: {
                "NAME" => node['hostname'],
                "ETH0_IP6" => node['prefix1_v6'] + node['n'],
                "ETH1_IP6" => node['prefix2_v6'] + node['n'],
                "ETH1_IP6_PREFIX" => node['prefix2_v6']
            }

            node_config.vm.provider :virtualbox do |vb|
                vb.gui = false
                vb.name = node['hostname']
                vb.linked_clone = true
                vb.memory = 1024
                vb.cpus = 2
            end
        end
    end
end